package main

import (
	_ "github.com/go-sql-driver/mysql"

	"bitbucket.org/CurtHealey/contentful-api/internal/config"
	"bitbucket.org/CurtHealey/contentful-api/internal/handlers"

	"net/http"

	"github.com/gin-contrib/cors"
	"github.com/gin-gonic/gin"
	"go.uber.org/zap"
)

func main() {

	logger, err := zap.NewProduction()
	if err != nil {
		logger.Fatal("Failed to get logger with error: %s", zap.Error(err))
	}

	db, err := config.GetDB()
	if err != nil {
		logger.Fatal("Failed to connect to database", zap.Error(err))
	}

	m := handlers.NewManager(db,logger)

	router := gin.Default()
	router.Use(cors.Default())

	router.POST("/validatetask", m.ValidateTask)

	err = http.ListenAndServe(":3000", router)
	if err != nil {
		logger.Fatal("Failed to connect to port", zap.Error(err))
	}

}
