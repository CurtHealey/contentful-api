module bitbucket.org/CurtHealey/contentful-api

go 1.15

require (
	github.com/alex-ant/envs v0.0.0-20180605211528-ff120f8dc147
	github.com/gin-contrib/cors v1.3.1
	github.com/gin-gonic/gin v1.7.2
	github.com/go-sql-driver/mysql v1.6.0
	github.com/jmoiron/sqlx v1.3.4
	github.com/olekukonko/tablewriter v0.0.5 // indirect
	go.uber.org/zap v1.18.1
)
