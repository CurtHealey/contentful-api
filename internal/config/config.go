package config

import (
	"flag"
	"log"

	"github.com/alex-ant/envs"
	_ "github.com/go-sql-driver/mysql"
	"github.com/jmoiron/sqlx"
)

func GetDB() (db *sqlx.DB, err error) {
	db, err = sqlx.Open("mysql", *DatabaseURL)
	return
}

var (
	DatabaseURL = flag.String("database-url", "fast:hJtvCn+TT]oE4TRTcAFth@tcp(patrik-staging-rdsclus.cluster-cadeka6550g6.eu-west-1.rds.amazonaws.com:3306)/?parseTime=true", "Database URL")
)

func init() {
	// Parse flags if not parsed already
	if !flag.Parsed() {
		flag.Parse()
	}

	// Determine and read environment variables
	flagsErr := envs.GetAllFlags()
	if flagsErr != nil {
		log.Fatal(flagsErr)
	}
}