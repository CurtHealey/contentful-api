package handlers

import (
	"bitbucket.org/CurtHealey/contentful-api/internal/structs"
	"net/http"

	"github.com/gin-gonic/gin"
	"github.com/jmoiron/sqlx"
	"go.uber.org/zap"
)

type Manager struct {
	db     *sqlx.DB
	logger *zap.Logger
}

func NewManager(db *sqlx.DB, logger *zap.Logger) *Manager {
	return &Manager{
		db:     db,
		logger: logger,
	}
}

func (m *Manager) ValidateTask(c *gin.Context) {

	var flag int
	var req structs.Request

	err := c.ShouldBindJSON(&req)
	if err != nil {
		m.logger.Fatal("Failed to bind request", zap.Error(err))
	}
	err = m.db.Get(&flag, req.Query)

	if err != nil {
		m.logger.Fatal("Failed to bind request", zap.Error(err))
	}

	response := structs.Response{
		Flag: flag,
	}

	c.JSON(http.StatusOK, response)

}
